# SPDX-FileCopyrightText: 2024-present Joseph Edwards VIII <jedwards@cieloconnects.com>
#
# SPDX-License-Identifier: MIT
__version__ = "0.1.0"
