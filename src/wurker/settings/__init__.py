import os
import json
import tomlkit

from slugify import slugify
# from dotenv import load_dotenv
from dataclasses import dataclass, asdict, field


@dataclass
class WurkerConf:
    """A data class for Wurker config."""

    name: str
    slug: str
    app_id: str
    env: str
    debug: bool
    log: str
    loglevel: str
    is_queen: bool


@dataclass
class DBConf:
    """A data class for DB config."""

    # Required fields
    database: str
    dbdriver: str

    # Optional driver-dependent fields
    hostname: str = ''
    username: str = ''
    password: str = field(default='', repr=False)
    port: int = 0

    # URL or DSN?
    url: str = ''
    dsn: str = ''


@dataclass
class UIConf:
    """A data class for UI config."""

    domain: str = ''


@dataclass
class LocalConf:
    """A data class for local data config."""

    dbdriver: str
    data_root: str
    seed_root: str
    seed_glob: str
    local_db: str
    hive_db: str
    ui_db: str
    api_auth: tuple


class Conf:
    """A slotted class to encapsulate settings."""

    __slots__ = [
        "__dict__",
        "conf_root", "conf_file",
        "wurker", "db", "ui", "local",
    ]

    # __slots__ = [
    #     "__dict__", "conf_dir", "conf_file", "app_root", "name", "slug", "app_id", "env", "debug",
    #     "log", "loglevel", "is_queen", "db_hostname", "db_username", "db_password", "db_database",
    #     "db_driver", "db_port", "local_data_dir", "local_db_database", "local_db_driver", "app_db_database",
    #     "seed_dir", "seed_schema_sql", "seed_data_glob", "ui_domain", "api_auth",
    # ]

    url_format = "{dbdriver}:///{database}".format
    dsn_format = "{dbdriver}://{username}:{password}@{hostname}:{port}/{database}".format

    # Possible config dirs
    allowed_conf_dirs = ['/usr/local/share/wurker', '~/.local/share/wurker',]
    conf_classes = {"wurker": WurkerConf, "db": DBConf, "ui": UIConf, "local": LocalConf,}

    def __init__(self):
        """Raise exceptions if config can't be determined."""

        # Init empty slots
        for slot in [s for s in self.__slots__ if not s.startswith("__")]:
            setattr(self, slot, None)

        # Do the rest...
        self._init_conf()
        self._init_api_auth()

    def _init_conf(self):
        """Determine the config root or die trying. Then load conf into objects."""

        conf_file = None
        for conf_dir in self.allowed_conf_dirs:
            conf_dir = os.path.abspath(os.path.expanduser(conf_dir))

            if os.path.exists(conf_dir) and os.path.exists(os.path.join(conf_dir, "wurker.toml")):
                self.conf_dir = conf_dir
                self.conf_file = os.path.join(conf_dir, "wurker.toml")
                break

        if not self.conf_file:
            raise Exception(
                f"Configuration file not found. Expected 'wurker.toml' in one of: {self.allowed_conf_dirs}"
            )

        # Load the config from TOML file and set slots
        with open(self.conf_file, 'r') as fp:
            lines = fp.read()

        conf = tomlkit.parse(lines)
        if not conf or "wurker" not in conf or "db" not in conf or "ui" not in conf:
            raise Exception("Configuration file is malformed.")

        # This little loop with throw exceptions for bad conf, since we're using slots & dataclasses
        for section, conf_class in self.conf_classes.items():
            setattr(self, section, conf_class(**conf[section]))

        # Do any section-specific config that needs doing
        self._init_conf_wurker()
        self._init_conf_db()
        self._init_conf_ui()
        self._init_conf_local()

    def _init_conf_wurker(self):
        """TODO Wurker section config and validation."""

        # Slugify Wurker name if no slug set.
        if self.wurker.name and not self.wurker.slug:
            self.wurker.slug = slugify(self.name)

    def _init_conf_db(self):
        """TODO Database section config and validation."""

    def _init_conf_ui(self):
        """TODO UI section config and validation."""

    def _init_conf_local(self):
        """TODO Local section config and validation."""

        if not self.local.dbdriver:
            self.local.dbdriver = 'sqlite'
        if not self.local.data_root:
            self.local.data_root = os.path.join(self.conf_root, "data")

        if not self.local.local_db:
            self.local.local_db = os.path.join(self.local.data_root, "wurker.db")
        if not self.local.hive_db:
            self.local.hive_db = os.path.join(self.local.data_root, "hive.db")
        if not self.local.ui_db:
            self.local.ui_db = os.path.join(self.local.data_root, "ui.db")

        if not self.local.seed_root:
            self.local.seed_root = os.path.join(self.conf_root, "etc", "seeds")
        if not self.local.seed_glob:
            self.local.seed_glob = "seed_data_[0-9][0-9][0-9].json"

    def _init_api_auth(self):
        """We need local .gitignored plaintext drone API auth, if applicable."""

        self.local.api_auth = None

        api_auth_json = os.path.join(self.local.data_root, "api_auth.json")
        if not os.path.exists(api_auth_json):
            return

        with open(api_auth_json, 'r') as f:
            users = json.load(f)

        if not users:
            return

        user = users.pop()
        if "username" not in user or "password" not in user:
            return

        self.local.api_auth = (user["username"], user["password"])

    @property
    def ConfDB(self) -> DBConf:
        """Return a DBConf for the primary config DB. May be a local sqlite DB."""

        if self.db.dbdriver == 'sqlite':
            url = self.url_format()

        kwargs = {
            "hostname": self.db_hostname,
            "username": self.db_username,
            "password": self.db_password,
            "database": self.db_database,
            "dbdriver": self.db_driver,
            "port": int(self.db_port) if self.db_port else None,
        }
        kwargs["dsn"] = self.dsn_format(**kwargs)

        return DBConf(**kwargs)

    @property
    def LocalDB(self) -> DBConf:
        url = (
            self.url_format(dbdriver=self.local_db_driver, database=self.local_db_database)
            if self.local_db_driver == "sqlite"
            else ''
        )
        return DBConf(dbdriver=self.local_db_driver, database=self.local_db_database, url=url)

    @property
    def AppDB(self) -> DBConf:
        url = self.url_format(dbdriver="sqlite", database=self.app_db_database)
        return DBConf(dbdriver="sqlite", database=self.app_db_database, url=url)

    @property
    def ApiAuth(self) -> tuple:
        return self.api_auth

    def asdict(self, attr:str=''):
        if not attr:
            return {k:getattr(self, k) for k in self.__slots__}

        db_conf = None
        if attr == "RemoteDB":
            db_conf = self.RemoteDB
        elif attr == "LocalDB":
            db_conf = self.LocalDB

        if not db_conf:
            return {}

        return asdict(db_conf)


# Look at us all bad ass!
conf = Conf()
wurker = conf.wurker
db = conf.db
ui = conf.ui
