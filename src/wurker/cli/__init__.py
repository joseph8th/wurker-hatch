import json
import click

from wurker.__about__ import __version__


@click.group(context_settings={"help_option_names": ["-h", "--help"]})
@click.version_option(version=__version__, prog_name="wurk")
@click.option("--log", default='stdout', type=click.Choice(['stdout','syslog']), help="Where to log.")
@click.pass_context
def wurk(ctx, log):
    """
    If cron and supervisor had a superpowered love child.

    Run `man wurk` for detailed usage.
    """

    ctx.ensure_object(dict)
    ctx.obj['log'] = log


# The 'admin' group
@wurk.group("admin")
@click.pass_context
def admin(ctx):
    """Administrate Wurker resources."""
    pass


## The 'admin.stinger' subgroup
@admin.group("stinger")
@click.pass_context
def stinger(ctx):
    """Manage injected Wurker resources."""
    pass

@stinger.command()
@click.option("--root", help="Path to root of stringers tree.")
@click.option("--slug", default="default", help="Stinger slug (default: 'default').")
@click.option("--node", default="queen", help="Colony node slug (default: 'queen').")
@click.option("--env", default="local", help="Environment slug (default: 'local').")
@click.pass_context
def sting(ctx, root, slug, node, env):
    """Inject resources from a Wurker Stinger repository."""

    click.echo(f"sting: {root=}, {slug=}, {node=}, {env=}")

    # Always include the default deps


@stinger.command()
@click.option("--drop-schema", is_flag=True, help="Also drop database schema.")
@click.pass_context
def unsting(ctx, drop_schema):
    """Remove injected resources from Wurker."""

    click.echo(f"unsting: {drop_schema=}")

@stinger.command()
@click.option("--root", help="Path to root of stringers tree.")
@click.option("--slug", default="default", help="Stinger slug (default: 'default').")
@click.option("--node", default="queen", help="Colony node slug (default: 'queen').")
@click.option("--env", default="local", help="Environment slug (default: 'local').")
@click.option("--drop-schema", is_flag=True, help="Also drop database schema.")
@click.pass_context
def resting(ctx, root, slug, node, env, drop_schema):
    """Re-inject resources from a Wurker Stinger repository."""

    click.echo(f"sting: {root=}, {slug=}, {node=}, {env=}, {drop_schema=}")


### The 'admin.stinger.extract' subsubgroup
EXTRACT_CHOICES = [
    'ALL', 'requirements.txt', '.env', 'seeds', 'models', 'bees',
    'settings', 'settings/bees', 'lib', 'lib/mail', 'ssl', 'data',
]

@stinger.group("extract")
@click.pass_context
def extract(ctx):
    """Extract resources from Wurker to a Stinger repository."""
    pass

@extract.command()
@click.option(
    "-r", "--resource", "resources", multiple=True, default=['ALL'], type=click.Choice(EXTRACT_CHOICES),
    help="Which resources to extract."
)
@click.option("--root", required=True, help="Root of stingers tree (ex: '../wurker-stingers').")
@click.option("--slug", required=True, help="Stinger slug (ex: 'default').")
@click.option("--node", required=True, help="Colony node slug (ex: 'queen').")
@click.option("--env", required=True, help="Environment slug (ex: 'local').")
@click.pass_context
def wurker(ctx, resources, root, slug, node, env):
    """Extract resources to a 'wurker' directory."""

    click.echo(f"sting: {resources=}, {root=}, {slug=}, {node=}, {env=}")

@extract.command()
@click.option(
    "-r", "--resource", "resources", multiple=True, default=['ALL'], type=click.Choice(EXTRACT_CHOICES),
    help="Which resources to extract."
)
@click.option("--root", required=True, help="Path to root of stringers tree.")
@click.option("--slug", required=True, help="Stinger slug (ex: 'default').")
@click.option("--node", required=True, help="Colony node slug (ex: 'queen').")
@click.option("--env", required=True, help="Environment slug (ex: 'local').")
@click.pass_context
def colony(ctx, resources, root, slug, node, env):
    """Extract resources to a 'colony' directory."""

    click.echo(f"sting: {resources=}, {root=}, {slug=}, {node=}, {env=}")


## The 'admin.test' subgroup
@admin.command()
@click.option("-v", "--verbose", is_flag=True, help="Run tests with verbosity.")
@click.pass_context
def test(ctx):
    """Run unit tests on local Wurker resources."""

    click.echo(f"test: {verbose=}")

@admin.command()
@click.pass_context
def seed(ctx):
    """Seed Wurker database with Stinger Seeds."""
    click.echo("seed")

@admin.command()
@click.pass_context
def service(ctx):
    """Create and install Wurker services."""
    click.echo("service")

@admin.command()
@click.argument("action", type=click.Choice(['reload', 'pause']))
@click.option("-d", "--drone", "drones", multiple=True, help="Slugs or ID of drone(s) to control (repeatable).")
@click.pass_context
def control(ctx, action, drones):
    """Reload or pause Wurker."""

    click.echo(f'control: {action=}, {drones=}')


# CRUD commands
@wurk.command()
@click.argument("choice", type=click.Choice(['bee', 'cron', 'map', 'command', 'ondemand',]))
@click.argument('infile', type=click.File('r'))
@click.option("-d", "--drone", "drones", multiple=True, help="Slug or ID of drone(s) to add to (repeatable).")
@click.pass_context
def add(ctx, choice, infile, drones):
    """Add Wurker definitions."""

    click.echo(f"add: {choice=}, {infile.name=}, {drones=}")

    data = json.load(infile)

    # TODO
    click.echo(f"{data=}")


@wurk.command()
@click.argument("choice", type=click.Choice([
    'status', 'bee', 'cron', 'command', 'ondemand', 'scheduled', 'supervised', 'hive',
]))
@click.option("--slug", help="Filter by slug (only: command, cron, bee).")
@click.option("--any", 'any_', is_flag=True, help="Also show orphaned or widowed (only: command, cron, bee).")
@click.option("-d", "--drone", "drones", multiple=True, help="Slug or ID of drone(s) to show (repeatable).")
@click.pass_context
def show(ctx, choice, slug, any_, drones):
    """Show Wurker definitions."""

    click.echo(f"show: {choice=}, {slug=}, {any_=}, {drones=}")


@wurk.command()
@click.argument("choice", type=click.Choice(['bee', 'cron', 'command',]))
@click.argument("key")
@click.argument('infile', type=click.File('r'))
@click.option("-d", "--drone", "drones", multiple=True, help="Slug or ID of drone(s) to edit on (repeatable).")
@click.pass_context
def edit(ctx, choice, key, infile, drones):
    """Edit Wurker definitions."""

    click.echo(f"edit: {choice=}, {key=}, {infile.name=}, {drones=}")

    data = json.load(infile)

    # TODO
    click.echo(f"{data=}")


@wurk.command()
@click.argument("choice", type=click.Choice(['bee', 'cron', 'command', 'ondemand',]))
@click.argument("key")
@click.option("-d", "--drone", "drones", multiple=True, help="Slug or ID of drone(s) to remove from (repeatable).")
@click.pass_context
def remove(ctx, choice, key, drones):
    """Remove Wurker definitions."""

    click.echo(f"remove: {choice=}, {key=}, {drones=}")


# Runtime commands
@wurk.command()
@click.argument("choice", type=click.Choice(['bee', 'cron', 'ondemand', 'scheduled', 'supervised',]))
@click.option("--slug", help="Filter by slug (only: cron, bee).")
@click.pass_context
def run(ctx, choice, slug):
    """Run Wurker Bees."""

    click.echo(f"run: {choice=}, {slug=}")


@wurk.command()
@click.argument("choice", type=click.Choice(['bee', 'cron', 'ondemand', 'scheduled', 'supervised', 'hive',]))
@click.option("--slug", help="Filter by slug (only: cron, bee).")
@click.option("-d", "--drone", "drones", multiple=True, help="Slug or ID of drone(s) to kill on (repeatable).")
@click.pass_context
def kill(ctx, choice, slug, drones):
    """Kill Wurker Bees."""

    click.echo(f"kill: {choice=}, {slug=}, {drones=}")


@wurk.command()
@click.argument("choice", type=click.Choice(['bee', 'cron',]))
@click.option("--slug", help="Filter by slug.")
@click.option("--start", help="Optional start date/time.")
@click.option("--end", help="Optional end date/time.")
@click.option("--page", type=int, help="Pagination page number.")
@click.option("--page-limit", type=int, help="Records per page.")
@click.option("--sort", help="Sort by column.")
@click.option("--sort-order", type=click.Choice(['asc', 'desc']), help='Sort direction.')
@click.option("-d", "--drone", "drones", multiple=True, help="Slug or ID of drone(s) to report on (repeatable).")
@click.pass_context
def report(ctx, choice, slug, start, end, page, page_limit, sort, sort_order, drones):
    """Report on Wurker Bees."""

    click.echo(f"report: {choice=}, {slug=}, {start=}, {end=}, {page=}, {page_limit=}, {sort=}, {sort_order=}, "
               f"{drones=}")
