import os
import pytest

from wurker.core import Wurkman
from wurker.util import BotArgs
from wurker.subcommands.show import run

@pytest.fixture
def app_root():
    return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def test_hive(app_root):
    """If a Wurker is running, this test SHOULD fail. Stop all wurkers first."""

    args = BotArgs(
        app_root = app_root,
        subcommand = "show",
        hive = True,
    )
    wurkman = Wurkman(args)
    hive = run(wurkman)
    assert len(hive) == 0

def test_command(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "show",
        command = "test_bee",
    )
    wurkman = Wurkman(args)
    commands = run(wurkman)

    assert len(commands) == 1
    assert commands[0]["app_id"] == wurkman.app_id
    assert commands[0]["name"] == "Test Bee"
    assert commands[0]["slug"] == "test_bee"
    assert commands[0]["description"] == "A shell script for testing Wurker Bee commands"
    assert commands[0]["command"] == os.path.join(app_root, "scripts", "test_bee.sh")
    assert commands[0]["is_module"] == False

def test_cron(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "show",
        cron = "supervised",
    )
    wurkman = Wurkman(args)
    crons = run(wurkman)

    assert len(crons) == 1
    assert crons[0]["app_id"] == wurkman.app_id
    assert crons[0]["name"] == "Supervised Daemon"
    assert crons[0]["slug"] == "supervised"
    assert crons[0]["cron"] is None

def test_bee(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "show",
        bee = "test_supervised_bee",
        any = True,
    )
    wurkman = Wurkman(args)
    bees = run(wurkman)

    assert len(bees) == 1
    assert bees[0]["app_id"] == wurkman.app_id
    assert bees[0]["name"] == "Test Supervised Bee"
    assert bees[0]["slug"] == "test_supervised_bee"
    assert bees[0]["description"] == "Test supervised Bee script"
    assert isinstance(bees[0]["args"], list)
    assert bees[0]["args"][0] == "-l"
    assert bees[0]["args"][1] == "/tmp/test_supervised_bee.log"
    assert bees[0]["args"][2] == "-i"
    assert bees[0]["args"][3] == "10"
    assert bees[0]["quiet_restart"] == False
    assert bees[0]["enabled"] == False
    assert bees[0]["module"] is None
    assert bees[0]["command_slug"] == "test_bee"
    assert bees[0]["command"] == os.path.join(app_root, "scripts", "test_bee.sh")
